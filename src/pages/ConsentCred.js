import React, { useMemo, useEffect, useState } from 'react';
import './ConsentCred.css'
import Sidebar from '../components/Sidebar';

import { Link } from 'react-router-dom';

import { useTable, useSortBy, useExpanded, usePagination } from 'react-table';
import { AiOutlineRight, AiOutlineLeft } from "react-icons/ai";
import { IoClose } from "react-icons/io5";
import sort from '../img/icon/sort.svg';
import sortDown from '../img/icon/sortDown.svg';
import sortUp from '../img/icon/sortUp.svg';

import { queryConsentData } from '../api/backend';
import { formatDateTime } from '../reducer';


function ConsentCred() {

    const [consentCredData, setConsentCredData] = useState([]);

    useEffect(() => {
        async function fetchData() {
            let Id = '123';
            let result = await queryConsentData(Id);
            // console.log(result);
            if (result) {
                setConsentCredData(result.data);
            }
        }
        fetchData();
    }, [])

    function submitConsent(did) {
        console.log(did);
    }

    function ConsentCredStatusHandler(status) {
        switch (status) {
            case 0:
                return (
                    <div style={{ color: '#c56612' }}>
                        Pending
                    </div>
                )
            case 1:
                return (
                    <div style={{ color: '#009621' }}>
                        Verified
                    </div>
                )
        }
    }

    let data;
    data = useMemo(() => consentCredData)

    const columns = useMemo(
        () => [
            {
                Header: 'LAST UPDATE',
                accessor: 'updateTime', // accessor is the "key" in the data
                Cell: ({ cell: { value } }) =>
                    formatDateTime(value)
            },
            {
                Header: 'DATA OWNER',
                accessor: 'dataOwner',
            },
            {
                Header: 'CONSENT DID',
                accessor: 'consentdid',
            },
            {
                Header: 'DATA PROVIDER',
                accessor: 'dataProvider',
            },
            {
                Header: 'ACTION',
                id: 'expander',
                accessor: 'data',
                Cell: ({ cell: { row } }) => {
                    // console.log(JSON.parse(row.values.expander).status);
                    let status = JSON.parse(row.values.expander).status;
                    let action;
                    (status == 0)
                        ? action = <span style={{color:"#009621"}}>Consent Submitted</span>
                        : action = <span {...row.getToggleRowExpandedProps()}>
                            {row.isExpanded ? <IoClose /> : <span className="view-btn">View</span>}
                        </span>
                    return (
                        action
                    )

                },
            },
        ],
        []
    )

    function Table({ columns, data, renderRowSubComponent }) {
        // Use the state and functions returned from useTable to build your UI
        const {
            getTableProps,
            getTableBodyProps,
            headerGroups,
            prepareRow,
            page,
            rows,
            canPreviousPage,
            canNextPage,
            pageOptions,
            pageCount,
            gotoPage,
            nextPage,
            previousPage,
            setPageSize,
            visibleColumns,
            state: { pageIndex, pageSize, expanded },
        } = useTable(
            {
                columns,
                data,
                initialState: {
                    pageIndex: 0, sortBy: [
                        {
                            id: 'updateTime',
                            desc: true
                        }
                    ]
                },
            },
            useSortBy,
            useExpanded,
            usePagination
        )


        // render table UI
        return (
            <>
                <table {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                        <div className="tableHeader">
                                            <div className="pr-1">{column.render('Header')}</div>
                                            <span>
                                                {column.isSorted
                                                    ? column.isSortedDesc
                                                        ? <img src={sortDown} width="20" height="20" />
                                                        : <img src={sortUp} width="20" height="20" />
                                                    : <img src={sort} width="20" height="20" />}
                                            </span>
                                        </div>
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                    {/* <tbody {...getTableBodyProps()}>
                        {page.map((row, i) => {
                            prepareRow(row)
                            return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                })}
                            </tr>
                            )
                        })}
                    </tbody> */}
                    <tbody {...getTableBodyProps()}>
                        {page.map((row, i) => {
                            prepareRow(row)
                            return (
                                <React.Fragment {...row.getRowProps()}>
                                    <tr>
                                        {row.cells.map(cell => {
                                            return (
                                                <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                            )
                                        })}
                                    </tr>
                                    {row.isExpanded ? (
                                        <tr>
                                            <td colSpan={visibleColumns.length} style={{ padding: '0px' }}>
                                                {renderRowSubComponent({ row })}
                                            </td>
                                        </tr>
                                    ) : null}
                                </React.Fragment>
                            )
                        })}
                    </tbody>
                </table>
                {/* 
                Pagination can be built however you'd like. 
                This is just a very basic UI implementation:
                */}
                <div className="pagination">

                    <div>Showing 1 - {(rows.length <= 10) ? rows.length : pageSize} of {rows.length} results</div>
                    <div>
                        <span className="pr-1">Results per page:</span>
                        <select
                            value={pageSize}
                            onChange={e => {
                                setPageSize(Number(e.target.value))
                            }}
                            className="pageSelect"
                        >
                            {[10, 20, 30, 40, 50].map(pageSize => (
                                <option key={pageSize} value={pageSize}>{pageSize}
                                </option>
                            ))}
                        </select>
                    </div>



                    <div className="pages-controller">

                        <button className="pr-1" onClick={() => previousPage()} disabled={!canPreviousPage}>
                            {<AiOutlineLeft />}<span className="pl-1"> PREVIOUS</span>
                        </button>{' '}
                        <span className="pageInput-wrapper">
                            <input
                                type="number"
                                defaultValue={pageIndex + 1}
                                onChange={e => {
                                    const page = e.target.value ? Number(e.target.value) - 1 : 0
                                    gotoPage(page)
                                }}
                                className="pageInput"
                            />
                            of <span style={{ color: '#333' }}>{pageOptions.length}</span>
                        </span>
                        <button className="pl-1" onClick={() => nextPage()} disabled={!canNextPage}>
                            <span className="pr-1">NEXT</span> {<AiOutlineRight />}
                        </button>{' '}

                    </div>
                </div>
            </>
        )
    }

    // table detail wrapper
    const renderRowSubComponent = React.useCallback(
        ({ row }) => {
            let data = JSON.parse(row.values.expander);
            return (
                <div className="detail-wrapepr" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', background: '#f9fcfe', padding: '10px 20px', borderLeft: '5px solid #c56612' }}>
                    <div className="flex-row" style={{ display: 'flex', flexDirection: 'row' }}>
                        <div className="title" style={{ flex: '.6', fontSize: '22px', textAlign: "right", paddingRight: '15px', borderRight: '1px solid #E2E2E2', marginRight: '25px' }}>
                            Consent <br></br>Information
                        </div>
                        <div className="content" style={{ flex: '4' }}>
                            <div className="detail-group" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Data Consumer
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.consentInformation.dataConsumer}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Data Provider
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.consentInformation.dataProvider}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Data Owner
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.consentInformation.dataOwner}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        DID
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.consentInformation.did}
                                    </p>
                                </div>
                            </div>
                            <div className="detail-group" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', marginTop: '10px' }}>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Purpose
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.consentInformation.purpose}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Document Ref. No.
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.consentInformation.docRefNo}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Access Mode
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.consentInformation.accessMode}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Verification Status
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {ConsentCredStatusHandler(data.consentInformation.status)}
                                        {/* {data.consentInformation.status} */}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="action-wrapper" style={{ flex: '1' }}>
                        </div>
                    </div>
                    <div className="flex-row" style={{ display: 'flex', flexDirection: 'row', marginTop: '60px' }}>
                        <div className="title" style={{ flex: '.6', fontSize: '22px', textAlign: "right", paddingRight: '15px', borderRight: '1px solid #E2E2E2', marginRight: '25px' }}>
                            Data <br></br>Scope
                        </div>
                        <div className="content" style={{ flex: '4' }}>
                            <div className="detail-group" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Scope Type
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.dataScope.scopeType}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Document Type
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.dataScope.docType}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        UDR
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.dataScope.udr}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Access Mode
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.dataScope.accessMode}
                                    </p>
                                </div>
                            </div>
                            <div className="detail-group" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', marginTop: '10px' }}>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Start Date
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.dataScope.startDate}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        End Date
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.dataScope.endDate}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>
                                    <span style={{ color: '#9b9b9b', fontSize: '12px' }}>
                                        Query Ferquency Type
                                    </span>
                                    <p style={{ fontSize: '16px' }}>
                                        {data.dataScope.queryFrequencyType}
                                    </p>
                                </div>
                                <div className="info-group" style={{ paddingRight: '50px', width: '200px' }}>

                                </div>
                            </div>
                        </div>
                        <div className="action-wrapper" style={{ flex: '1', alignSelf: 'flex-end' }}>
                            <div onClick={() => submitConsent(data.consentInformation.did)} style={{ padding: '10px', background: '#036aa4', color: '#fff', fontSize: '16px', fontWeight: '500', textAlign: 'center', borderRadius: '6px', width: '175px', height: '40px', cursor: 'pointer' }}>SUBMIT CONSENT</div>
                        </div>
                    </div>

                    {/* <p>{data.consentInformation.dataConsumer}</p> */}

                </div>
            )
        },
        []
    )

    return (
        <>
            <div className="container">
                <div className="container-inner">
                    <Sidebar />
                    <div className="main-wrapper">
                        <div className="page-head">
                            <p className="page-title">
                                Consent Credentials
                        </p>
                            <div className="hr"></div>
                        </div>

                        <div className="filter-wrapper">
                            <div className="filter-form">
                                <input className="filter-search-input" placeholder="Search by DID" />
                                <div className="filter-select">
                                    <select name="dateRange" id="dateRange" style={{ width: '175px' }}>
                                        <option value="dateRange">Date Range</option>
                                    </select>
                                    <select name="dataOwner" id="dataOwner" style={{ width: '255px' }}>
                                        <option value="dataOwner">Data Owner</option>
                                    </select>
                                    <select name="dataProvider" id="dataProvider" style={{ width: '255px' }}>
                                        <option value="dataProvider">Data Provider</option>
                                    </select>
                                </div>

                            </div>

                            <div className="hr"></div>

                        </div>

                        <div className="consentCred-table-wrapper">
                            <div className="consentCredList-table-wrapper">
                                {consentCredData != '' ? <Table columns={columns} data={data} renderRowSubComponent={renderRowSubComponent} /> : 'No consent data yet'}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}

export default ConsentCred
