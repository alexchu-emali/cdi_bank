import React from 'react'
import './Login.css';
import { useHistory, useLocation } from 'react-router-dom';
import { useForm } from "react-hook-form";

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { login } from '../api/auth';

function Login() {
    const location = useLocation();
    const history = useHistory();
    const { register, control, errors, handleSubmit } = useForm();
    const onSubmit = async (data) => {
        // e.preventDefault();
        // let username = data.username;
        // let password = data.password;
        // let success = await login(username, password);
        // if (success === true) {
        //     let { from } = location.state || { from: { pathname: "/home" } };
        //     history.replace(from);
        // }
        history.replace('/');
    }

    return (
        <>
            <div className="login-container">
                <div className="login-wrapper">
                    <div className="title">
                        <p>CDI</p>
                        <span>Bank Portal</span>
                    </div>
                    <form className="login-form" onSubmit={handleSubmit(onSubmit)}>
                        <TextField
                            // required
                            error={errors.username ? true : false}
                            variant="outlined"
                            margin="normal"
                            // inputRef={(e) => {
                            //     register(e, { required: "Username is required", })
                            // }}

                            fullWidth
                            id="username"
                            label="Username"
                            name="username"
                            autoFocus
                            helperText={errors.username?.message}
                        />

                        <TextField
                            error={errors.password ? true : false}
                            variant="outlined"
                            margin="normal"
                            // inputRef={(e) => {
                            //     register(e, { required: "password is required", })
                            // }}
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            helperText={errors.password?.message}
                        />
                        <div className="btn-wrapper">
                        <button className="btn" type="submit">
                            Login
                        </button>
                        </div>
                    </form>
                </div>
            </div>

        </>
    )
}

export default Login
