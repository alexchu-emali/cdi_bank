import React, { useMemo, useEffect, useState } from 'react';
import './CdiConsent.css'
import Sidebar from '../components/Sidebar';

import { useTable, useSortBy, useExpanded, usePagination } from 'react-table';
import { AiOutlineRight, AiOutlineLeft } from "react-icons/ai";
import { IoClose } from "react-icons/io5";
import sort from '../img/icon/sort.svg';
import sortDown from '../img/icon/sortDown.svg';
import sortUp from '../img/icon/sortUp.svg';

import { queryCdiConsent } from '../api/backend';
import { formatDateTime } from '../reducer';

function CdiConsent() {
    const [cdiConsent, setCdiConsent] = useState([]);

    useEffect(() => {
        async function fetchData() {
            let Id = '123';
            let result = await queryCdiConsent(Id);
            // console.log(result);
            if (result) {
                setCdiConsent(result.data);
            }
        }
        fetchData();
    }, [])


    function requestCustomerData(did) {
        console.log('send request here');
        console.log(did);
    }

    let data;
    data = useMemo(() => cdiConsent)

    const columns = useMemo(
        () => [
            {
                Header: 'LAST UPDATE',
                accessor: 'updateTime', // accessor is the "key" in the data
                Cell: ({ cell: { value } }) =>
                    formatDateTime(value)
            },
            {
                Header: 'CONSENT DID',
                accessor: 'consentdid',
            },
            {
                Header: 'DATA PROVIDER',
                accessor: 'dataProvider',
            },
            {
                Header: 'STATUS',
                accessor: 'status',
                Cell: ({ cell: { value } }) =>
                    statusHandler(value)
            },
            {
                Header: 'ACTION',
                accessor: ({ status, consentdid }) => {
                    let action;
                    (status != 1)
                        ? action = <span style={{ color: "#036aa4" }}> - </span>
                        : action = <span onClick={() => requestCustomerData(consentdid)} style={{ color: "#036aa4", cursor: 'pointer' }}>Request Customer Data</span>
                    return action
                },
            },
        ],
        []
    )

    function Table({ columns, data, renderRowSubComponent }) {
        // Use the state and functions returned from useTable to build your UI
        const {
            getTableProps,
            getTableBodyProps,
            headerGroups,
            prepareRow,
            page,
            rows,
            canPreviousPage,
            canNextPage,
            pageOptions,
            pageCount,
            gotoPage,
            nextPage,
            previousPage,
            setPageSize,
            visibleColumns,
            state: { pageIndex, pageSize, expanded },
        } = useTable(
            {
                columns,
                data,
                initialState: {
                    pageIndex: 0, sortBy: [
                        {
                            id: 'updateTime',
                            desc: true
                        }
                    ]
                },
            },
            useSortBy,
            useExpanded,
            usePagination
        )

        // render table UI
        return (
            <>
                <table {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                        <div className="tableHeader">
                                            <div className="pr-1">{column.render('Header')}</div>
                                            <span>
                                                {column.isSorted
                                                    ? column.isSortedDesc
                                                        ? <img src={sortDown} width="20" height="20" />
                                                        : <img src={sortUp} width="20" height="20" />
                                                    : <img src={sort} width="20" height="20" />}
                                            </span>
                                        </div>
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                    {/* <tbody {...getTableBodyProps()}>
                        {page.map((row, i) => {
                            prepareRow(row)
                            return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                })}
                            </tr>
                            )
                        })}
                    </tbody> */}
                    <tbody {...getTableBodyProps()}>
                        {page.map((row, i) => {
                            prepareRow(row)
                            return (
                                <React.Fragment {...row.getRowProps()}>
                                    <tr>
                                        {row.cells.map(cell => {
                                            return (
                                                <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                            )
                                        })}
                                    </tr>
                                    {row.isExpanded ? (
                                        <tr>
                                            <td colSpan={visibleColumns.length} style={{ padding: '0px' }}>
                                                {renderRowSubComponent({ row })}
                                            </td>
                                        </tr>
                                    ) : null}
                                </React.Fragment>
                            )
                        })}
                    </tbody>
                </table>
                {/* 
                Pagination can be built however you'd like. 
                This is just a very basic UI implementation:
                */}
                <div className="pagination">

                    <div>Showing 1 - {(rows.length <= 10) ? rows.length : pageSize} of {rows.length} results</div>
                    <div>
                        <span className="pr-1">Results per page:</span>
                        <select
                            value={pageSize}
                            onChange={e => {
                                setPageSize(Number(e.target.value))
                            }}
                            className="pageSelect"
                        >
                            {[10, 20, 30, 40, 50].map(pageSize => (
                                <option key={pageSize} value={pageSize}>{pageSize}
                                </option>
                            ))}
                        </select>
                    </div>



                    <div className="pages-controller">

                        <button className="pr-1" onClick={() => previousPage()} disabled={!canPreviousPage}>
                            {<AiOutlineLeft />}<span className="pl-1"> PREVIOUS</span>
                        </button>{' '}
                        <span className="pageInput-wrapper">
                            <input
                                type="number"
                                defaultValue={pageIndex + 1}
                                onChange={e => {
                                    const page = e.target.value ? Number(e.target.value) - 1 : 0
                                    gotoPage(page)
                                }}
                                className="pageInput"
                            />
                            of <span style={{ color: '#333' }}>{pageOptions.length}</span>
                        </span>
                        <button className="pl-1" onClick={() => nextPage()} disabled={!canNextPage}>
                            <span className="pr-1">NEXT</span> {<AiOutlineRight />}
                        </button>{' '}

                    </div>
                </div>
            </>
        )
    }

    function statusHandler(status) {
        switch (status) {
            case 0:
                return (
                    <div className="status yellow">
                        ENDORSEMENT PENDING
                    </div>
                )
            case 1:
                return (
                    <div className="status green">
                        CONSENT ACCEPTED
                    </div>
                )
            case 2:
                return (
                    <div className="status purple">
                        DATA REQUEST PENDING
                    </div>
                )
            case 3:
                return (
                    <div className="status red">
                        FAILED TO VERIFY
                    </div>
                )
        }
    }

    return (
        <>
            <div className="container">
                <div className="container-inner">
                    <Sidebar />
                    <div className="main-wrapper">
                        <div className="page-head">
                            <p className="page-title">
                                CDI Consent
                        </p>
                            <div className="hr"></div>
                        </div>

                        <div className="filter-wrapper">
                            <div className="filter-form">
                                <input className="filter-search-input" placeholder="Search by DID" />
                                <div className="filter-select">
                                    <select name="dateRange" id="dateRange" style={{ width: '175px' }}>
                                        <option value="dateRange">Date Range</option>
                                    </select>
                                    <select name="dataOwner" id="dataOwner" style={{ width: '255px' }}>
                                        <option value="dataOwner">Data Owner</option>
                                    </select>
                                    <select name="dataProvider" id="dataProvider" style={{ width: '255px' }}>
                                        <option value="dataProvider">Data Provider</option>
                                    </select>
                                </div>

                            </div>

                            <div className="hr"></div>

                        </div>

                        <div className="cdiConsent-table-wrapper">
                            <div className="cdiConsentList-table-wrapper">
                                {cdiConsent != '' ? <Table columns={columns} data={data} /> : 'No consent data yet'}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}

export default CdiConsent
