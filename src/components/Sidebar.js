import React from 'react';
import './Sidebar.css';
import { Link, NavLink,useHistory } from 'react-router-dom';

import { AiOutlineHome, AiOutlineFileText, AiOutlineImport, AiOutlineEdit } from 'react-icons/ai';
import { CgPlayListCheck } from 'react-icons/cg';
import {logOut} from '../api/auth';
import consentIcon from '../img/consentCred.png';
import cdi_consent from '../img/cdi_consent.png';
import customerData from '../img/customerData.png';
import logoutIcon from '../img/logout.png';

function Sidebar() {
    const history = useHistory();
    function logout(){
        console.log('logout');
        history.push('/login');
    }
    return (
        <>
            <div className="sidebar-wrapper">
                <ul className={"nav-options"}>
                    {/* <li className="title">
                        Dashboard
                    </li>
                    <NavLink activeClassName="active" to="/home">
                        <li className="option">
                            <AiOutlineHome /><span>Overview</span>
                        </li>
                    </NavLink> */}
                    <li className="title">
                        <p>CDI</p>
                        <span>Bank Portal</span>
                    </li>
                    <li className="brand">
                        <div className="brand-inner">
                            <div className="icon">
                                <img></img>
                            </div>
                            <div className="title">Standard<br></br> Chartered</div>
                        </div>
                    </li>

                    <NavLink activeClassName="active" to="/consentCred">
                        <li className="option">
                            <div className="icon"><img src={consentIcon} ></img></div><span>Consent Credential</span>
                        </li>
                    </NavLink>
                    <NavLink activeClassName="active" to="/cdiConsent">
                        <li className="option">
                        <div className="icon"><img src={cdi_consent} ></img></div><span>CDI Consent</span>
                        </li>
                    </NavLink>
                    <NavLink activeClassName="active" to="/customerData">
                        <li className="option">
                        <div className="icon"><img src={customerData} ></img></div><span>Customer Data</span>
                        </li>
                    </NavLink>

                    {/* <li className="title">
                        Log
                    </li>

                    <NavLink activeClassName="active" to="/logRecord">
                        <li className="option">
                            <AiOutlineEdit /><span>Log Record</span>
                        </li>
                    </NavLink> */}
                    {/* <li className="title">
                        Setting
                    </li> */}
                    <Link to="/login" onClick={()=>logout()}>
                        <li className="option logout">
                        <div className="icon"><img src={logoutIcon} ></img></div><span>Log Out</span>
                        </li>
                    </Link>
                </ul>
            </div>

        </>
    )
}

export default Sidebar
