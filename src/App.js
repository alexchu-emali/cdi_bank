import logo from './logo.svg';
import './App.css';
import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import Login from './pages/Login';
import ConsentCred from './pages/ConsentCred';
import jwt from 'jsonwebtoken';
import CdiConsent from './pages/CdiConsent';
import CustomerData from './pages/CustomerData';

function App() {

  function isLoggedIn() {
    console.log("checklogn");
    let loginDate = localStorage.getItem("loginDate");
    let id_token = localStorage.getItem("access_token");
    let decoded = jwt.decode(id_token);

    if (loginDate == null || id_token == null) {
      localStorage.removeItem('userInfo');
      localStorage.removeItem('access_token');
      localStorage.removeItem('userId');
      localStorage.removeItem('loginDate');
      return false;
    }

    let loginData = Number(localStorage.getItem("loginDate")) + 24 * 3600 * 1000 * 30;
    let nowDate = new Date().getTime();

    if (!Array.isArray(decoded.authorities) || !decoded.authorities.includes("ROLE_ISSUER")) {
      localStorage.removeItem('userInfo');
      localStorage.removeItem('access_token');
      localStorage.removeItem('userId');
      localStorage.removeItem('loginDate');
      return false;
    }

    if (loginData < nowDate) {
      localStorage.removeItem('userInfo');
      localStorage.removeItem('access_token');
      localStorage.removeItem('userId');
      localStorage.removeItem('loginDate');
      localStorage.removeItem("loginDate");
      return false;
    }

    return true;
  }

  function PrivateRoute({ component: Component, ...rest }) {
    // let auth = useAuth();
    let auth = isLoggedIn();
    return (
      <Route {...rest} render={props => (
        auth ?
          <Component {...props} />
          : <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
      )} />
    );
  }

  

  return (
    <>
      <Router basename="/uat/cdi_bank">
        <div className="app">
          <Switch>
            {/* <PrivateRoute component={ConsentCred} path="/consentCred" /> */}
            <Route component={CustomerData} path="/customerData" />
            <Route component={CdiConsent} path="/cdiConsent" />
            <Route component={ConsentCred} path="/consentCred" />
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/">
              <Redirect to="/consentCred" />
            </Route>
          </Switch>
        </div>

      </Router>
    </>
  );
}



export default App;
